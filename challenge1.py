#!/usr/bin/python

import re
import sys
import os

logStart = re.compile("^.*[0-9]{8} [0-9]{4}")

realfile=open("hosts.real", "r")
ip=open("ip.txt", "w")
tmp=open("temp.txt", "w")

for line in realfile:

    removehash = '#'
    data = line.split(removehash)[0]
    newLine = data.split()

    tmp.write(" ".join(newLine)+"\n")

    try:
        print(newLine[-1])
        ip.write(newLine[0]+"\n")
    except:
        pass

realfile.close()
ip.close()
tmp.close()


os.remove("hosts.real")
os.rename("temp.txt", "hosts.real")
