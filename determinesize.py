#!/usr/bin/python

import re

datafile=open("sample_dataset2.csv", "r")
datafile.readline() #skips the first line

ticker=date=vol=0
open=[0,0]
high=[0,0]
low=[0,0]
close=[0,0]

for line in datafile:
    cells=line.split(",")

    if len(cells[0]) > ticker:
        ticker=len(cells[0])

    if len(cells[1]) > date:
        date=len(cells[1])

    number=cells[2].split(".")
    if len(cells[2]) > open[0]:
        open[0]=(len(cells[2]))
    try:
        if len(number[1]) > open[1]:
            open[1]=(len(number[1]))
    except:
        pass

    number2=cells[3].split(".")
    if len(cells[3]) > high[0]:
        high[0]=(len(cells[3]))

    try:
        if len(number2[1]) > high[1]:
            high[1]=(len(number2[1]))
    except:
        pass

    number3=cells[4].split(".")
    if len(cells[4]) > low[0]:
        low[0]=(len(cells[4]))

    try:
        if len(number3[1]) > low[1]:
            low[1]=(len(number3[1]))
    except:
        pass

    number4=cells[5].split(".")
    if len(cells[5]) > close[0]:
        close[0]=(len(cells[5]))
    try:
        if len(number4[1]) > close[1]:
            close[1]=(len(number4[1]))
    except:
        pass

    if len(cells[6]) > vol:
         vol=len(cells[6])

print("Ticket Size: ", ticker)
print("Date size: ", date)
print("Open size: ", open[0], " Decimal: ", open[1])
print("High size: ", high[0], " Decimal: ", high[1])
print("Low size: ", low[0], " Decimal: ", low[1])
print("Close size: ", close[0], " Decimal: ", close[1])
print("Volume: ", vol)
