-- CREATE PROCEDURE volumewp
/*DECLARE @INDATE varchar(10);
SET @INDATE ='01/05/2010';
select convert (date, @indate,103) */

DECLARE @inputdate varchar(12)
set @inputdate = '201011100900';


SELECT SUM(vol*[close])/SUM(vol) as vwp,
	substring (@inputdate, 7, 2)+ '/'+
	substring (@inputdate, 5, 2)+'/' +substring (@inputdate, 1, 4) as "date" 
	,substring(@inputdate, 9,4) as [start time], substring(@inputdate, 9,4)+500 as [end time]
FROM sample_dataset2 
group by ticker



/*select convert(datetime,stuff(stuff([date],9,0, ' '),12,0,':')) 
from sample_dataset2 */
